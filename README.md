# Birthday Reminder

A simple app to show upcoming birthdays in a widget. It works completely offline using
contacts saved in local address book only.

It displays up to two contacts in the widget.

![Widget Demo](img/widget_preview.png "Birthday Reminder Widget Demo")

## License

This software is free software and licensed under GNU GPL v3. See [LICENSE.md](LICENSE.md) for
full license text.

## System requirement

Android 8.0 (SDK version 26+)

Tested with:
* Android 8.0
* Android 9.0
* Android 10.0

## Download APK

Of course, you can build the software by your own using Android SDK.

For these who just want a ready-to-use precompiled version, please download latest version: [birthday-reminder-1.1.0.apk](app/release/birthday-reminder-1.1.0.apk)

## Widget

Find a free 4x1 area on your home screen to place the widget.

When placing the widget you can configure it:

* how many days before the contact will appear in this widget
  * min: 0 days before - show contact on birthday only
  * max: 50 days before - show contact 50 days before birthday
* theme
  * light: dark text on light background
  * dark: light text on dark background
* background
  * none
  * transparent
  * opaque

On first start you need to allow permission READ_CONTACTS because the contacts are the
source to fetch birthdays from.

## Main screen

The main screen - available by tapping the widget - will list all phone contacts with birthdays
as a list. It also allows to jump to the contact directly by tapping the list entry.

## How it looks

| Widget in action | Configuration Screen | Main Screen |
|:----------------:|:--------------------:|:-----------:|
| ![Widget Demo](img/widget.png "Birthday Reminder Widget demo") | ![Configuration Screen Demo](img/option_screen.png "Configuration screen") | ![Main Screen Demo](img/main_screen.png "Main screen") |

## Used resources

* App Icon resource: https://www.svgrepo.com/svg/20280/birthday-cake
* Slider icon resource: Android Studio icon pack
