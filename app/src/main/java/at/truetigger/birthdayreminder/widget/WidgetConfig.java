package at.truetigger.birthdayreminder.widget;

import androidx.annotation.NonNull;

public class WidgetConfig {

    private final int daysUntil;
    private final WidgetTheme theme;
    private final WidgetBackground background;

    public WidgetConfig(int daysUntil, WidgetTheme theme, WidgetBackground background) {
        this.daysUntil = daysUntil;
        this.theme = theme;
        this.background = background;
    }

    public int getDaysUntil() {
        return daysUntil;
    }

    public WidgetTheme getTheme() {
        return theme;
    }

    public WidgetBackground getBackground() {
        return background;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("WidgetConfig[daysUntil=%d, theme=%s, background=%s", daysUntil, theme.name(), background.name());
    }
}
