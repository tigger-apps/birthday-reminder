package at.truetigger.birthdayreminder.widget;

public enum WidgetTheme {
    LIGHT,
    DARK,
}
