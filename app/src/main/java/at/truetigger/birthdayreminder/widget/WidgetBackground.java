package at.truetigger.birthdayreminder.widget;

public enum WidgetBackground {
    NONE,
    TRANSPARENT,
    OPAQUE,
}
