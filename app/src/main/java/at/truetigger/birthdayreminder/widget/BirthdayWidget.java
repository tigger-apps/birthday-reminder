package at.truetigger.birthdayreminder.widget;

import android.Manifest;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import at.truetigger.birthdayreminder.R;
import at.truetigger.birthdayreminder.activities.ConfigureActivity;
import at.truetigger.birthdayreminder.activities.MainActivity;
import at.truetigger.birthdayreminder.helper.BirthdayDataExtractor;
import at.truetigger.birthdayreminder.helper.BirthdayFormatter;
import at.truetigger.birthdayreminder.model.BirthdayRecord;

public class BirthdayWidget extends AppWidgetProvider {

    public static final String RECONFIGURE_ACTION = "at.truetigger.birthdayreminder.APPWIDGET_RECONFIGURE";

    private static final String TAG = "BirthdayWidget";

    private final BirthdayDataExtractor birthdayDataExtractor = new BirthdayDataExtractor();

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.d(TAG, "Updating widgets: " + Arrays.toString(appWidgetIds));
        for (int appWidgetId : appWidgetIds) {
            updateWidget(context, appWidgetManager, appWidgetId);
        }
        Toast.makeText(context, context.getResources().getText(R.string.widget_refresh_done), Toast.LENGTH_SHORT).show();
    }

    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getExtras() != null) {
            final int appWidgetId = intent.getExtras().getInt(AppWidgetManager.EXTRA_APPWIDGET_ID);

            if (intent.getAction().equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                updateWidget(context, appWidgetManager, appWidgetId);
                Log.d(TAG, "WidgetUpdateIntent received: " + intent);

            } else if(intent.getAction().equals(RECONFIGURE_ACTION)) {
                Log.d(TAG, "starting reconfigure for widget #" + appWidgetId);
                Intent intentConfig = new Intent(context, ConfigureActivity.class);
                intentConfig.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intentConfig.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
                intentConfig.putExtra(ConfigureActivity.RECONFIGURE_WIDGET, true);
                context.startActivity(intentConfig);
            }
        }
    }

    private void updateWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {

        if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
            Log.d(TAG, "update widget #" + appWidgetId);
            WidgetConfig config = loadWidgetConfig(context, appWidgetId);
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget);
            setTheme(context, remoteViews, config.getTheme());
            setBackground(context, remoteViews, config.getBackground(), config.getTheme());

            final boolean needPermission = (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED);

            if (needPermission) {
                showNeedPermission(remoteViews);
            } else {
                ZonedDateTime now = ZonedDateTime.now();
              final List<BirthdayRecord> birthdayRecords = loadBirthdays(context.getContentResolver(), now, config.getDaysUntil());
                showBirthdays(remoteViews, birthdayRecords, now, context);
            }

            setupWidgetActions(context, remoteViews, appWidgetId);
            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        }
    }

    private @NonNull
    List<BirthdayRecord> loadBirthdays(@NonNull ContentResolver contentResolver, ZonedDateTime now, int daysUntil) {
        Log.d(TAG, "Loading birthdays");
        List<BirthdayRecord> filteredBirthdays = new ArrayList<>();
        for (BirthdayRecord birthdayRecord : birthdayDataExtractor.extractBirthdayFromContacts(contentResolver)) {
            Log.d(TAG, "Found birthday: " + birthdayRecord);
            if (birthdayRecord.getDaysUntil(now) > daysUntil) {
                Log.d(TAG, "Reaching limit of " + daysUntil);
                break;
            }
            Log.d(TAG, "daysUntilNow: " + birthdayRecord.getDaysUntil(now));
            filteredBirthdays.add(birthdayRecord);
        }
        Log.d(TAG, "Return " + filteredBirthdays.size() + " birthday(s)");
        return filteredBirthdays;
    }

    private void setupWidgetActions(Context context, RemoteViews remoteViews, int appWidgetId) {

        // call main activity on click on widget
        Intent intentMain = new Intent(context, MainActivity.class);
        PendingIntent callMainActivityIntent = PendingIntent.getActivity(context, 0, intentMain, 0);
        remoteViews.setOnClickPendingIntent(R.id.widgetCanvas, callMainActivityIntent);

        // trigger refresh broadcast for specific widget
        Intent intentUpdate = new Intent(context, BirthdayWidget.class);
        intentUpdate.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intentUpdate.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, new int[]{appWidgetId});
        PendingIntent callUpdateIntent = PendingIntent.getBroadcast(context, appWidgetId, intentUpdate, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.btnInlineReload, callUpdateIntent);

        // trigger configuration for specific widget
        Intent intentConfig = new Intent(context, BirthdayWidget.class);
        intentConfig.setAction(RECONFIGURE_ACTION);
        intentConfig.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intentUpdate.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, new int[]{appWidgetId});
        PendingIntent callConfigIntent = PendingIntent.getBroadcast(context, 0, intentConfig, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.btnInlineConfig, callConfigIntent);
    }

    private void showNeedPermission(RemoteViews remoteViews) {
        remoteViews.setInt(R.id.widgetNoPermissionInfo, "setVisibility", View.VISIBLE);
        remoteViews.setInt(R.id.widgetNoBirthdays, "setVisibility", View.GONE);
        remoteViews.setInt(R.id.widgetFirstBirthday, "setVisibility", View.GONE);
        remoteViews.setInt(R.id.widgetSecondBirthday, "setVisibility", View.GONE);
    }

    private void showBirthdays(RemoteViews remoteViews, List<BirthdayRecord> birthdayRecords, ZonedDateTime now, Context context) {
        remoteViews.setInt(R.id.widgetNoPermissionInfo, "setVisibility", View.GONE);
        int numRecords = birthdayRecords.size();
        Log.d(TAG, "Write found birthdays: " + numRecords);
        if (numRecords == 0) {
            remoteViews.setInt(R.id.widgetNoBirthdays, "setVisibility", View.VISIBLE);
            remoteViews.setInt(R.id.widgetFirstBirthday, "setVisibility", View.GONE);
            remoteViews.setInt(R.id.widgetSecondBirthday, "setVisibility", View.GONE);
        } else {
            remoteViews.setInt(R.id.widgetNoBirthdays, "setVisibility", View.GONE);
            remoteViews.setTextViewText(R.id.widgetFirstBirthday, formatBirthdayInfo(birthdayRecords.get(0), now, context));
            remoteViews.setInt(R.id.widgetFirstBirthday, "setVisibility", View.VISIBLE);
            if (numRecords >= 2) {
                remoteViews.setTextViewText(R.id.widgetSecondBirthday, formatBirthdayInfo(birthdayRecords.get(1), now, context));
                remoteViews.setInt(R.id.widgetSecondBirthday, "setVisibility", View.VISIBLE);
            } else {
                remoteViews.setInt(R.id.widgetSecondBirthday, "setVisibility", View.GONE);
            }
        }
    }

    private String formatBirthdayInfo(BirthdayRecord birthdayRecord, ZonedDateTime now, Context context) {
        return birthdayRecord.getContactName() + ": "
                + BirthdayFormatter.formatDate(birthdayRecord, now, context)
                + " (" + BirthdayFormatter.formatDaysLeft(birthdayRecord.getDaysUntil(now), context) + ")";
    }

    private WidgetConfig loadWidgetConfig(Context context, int appWidgetId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(ConfigureActivity.SHARED_PREFS_BUNDLE, Context.MODE_PRIVATE);
        final int daysUntil = sharedPreferences.getInt(ConfigureActivity.KEY_DAYS_UNTIL + appWidgetId, 0);
        final WidgetTheme widgetTheme = WidgetTheme.valueOf(sharedPreferences.getString(ConfigureActivity.KEY_THEME + appWidgetId, WidgetTheme.LIGHT.name()));
        final WidgetBackground widgetBackground = WidgetBackground.valueOf(sharedPreferences.getString(ConfigureActivity.KEY_BACKGROUND + appWidgetId, WidgetBackground.TRANSPARENT.name()));
        WidgetConfig widgetConfig = new WidgetConfig(daysUntil, widgetTheme, widgetBackground);
        Log.d(TAG, "loaded widgetConfig for widgetId=" + appWidgetId + ": " + widgetConfig);
        return widgetConfig;
    }

    private void setTheme(Context context, RemoteViews remoteViews, WidgetTheme widgetTheme) {
        final int headlineColor = ContextCompat.getColor(context, widgetTheme == WidgetTheme.DARK ? R.color.widgetHeadlineDark : R.color.widgetHeadlineLight);
        final int noPermissionColor = ContextCompat.getColor(context, widgetTheme == WidgetTheme.DARK ? R.color.widgetMissingPermissionsDark : R.color.widgetMissingPermissionsLight);
        final int textColor = ContextCompat.getColor(context, widgetTheme == WidgetTheme.DARK ? R.color.widgetTextDark : R.color.widgetTextLight);
        final int refreshIcon = (widgetTheme == WidgetTheme.DARK ? R.drawable.ic_refresh_dark : R.drawable.ic_refresh_light);
        final int configIcon = (widgetTheme == WidgetTheme.DARK ? R.drawable.ic_config_dark : R.drawable.ic_config_light);
        remoteViews.setInt(R.id.widgetTitle, "setTextColor", headlineColor);
        remoteViews.setInt(R.id.widgetNoPermissionInfo, "setTextColor", noPermissionColor);
        remoteViews.setInt(R.id.widgetNoBirthdays, "setTextColor", textColor);
        remoteViews.setInt(R.id.widgetFirstBirthday, "setTextColor", textColor);
        remoteViews.setInt(R.id.widgetSecondBirthday, "setTextColor", textColor);
        remoteViews.setImageViewResource(R.id.btnInlineReload, refreshIcon);
        remoteViews.setImageViewResource(R.id.btnInlineConfig, configIcon);
        Log.d(TAG, "Theme " + widgetTheme.name() + " set");
    }

    private void setBackground(Context context, RemoteViews remoteViews, WidgetBackground widgetBackground, WidgetTheme theme) {
        final int backgroundColor;
        switch (widgetBackground) {
            case TRANSPARENT:
                backgroundColor = theme == WidgetTheme.DARK ? R.color.widgetBackgroundTransparentDark : R.color.widgetBackgroundTransparentLight;
                break;
            case OPAQUE:
                backgroundColor = theme == WidgetTheme.DARK ? R.color.widgetBackgroundOpaqueDark : R.color.widgetBackgroundOpaqueLight;
                break;
            default:
                backgroundColor = R.color.widgetBackgroundNone;
        }
        remoteViews.setInt(R.id.widgetCanvas, "setBackgroundColor", ContextCompat.getColor(context, backgroundColor));
        Log.d(TAG, "Background " + widgetBackground.name() + " set (with theme " + theme.name() + ")");
    }
}
