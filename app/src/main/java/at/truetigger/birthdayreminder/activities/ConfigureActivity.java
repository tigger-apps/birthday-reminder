package at.truetigger.birthdayreminder.activities;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;

import androidx.appcompat.app.AppCompatActivity;

import at.truetigger.birthdayreminder.R;
import at.truetigger.birthdayreminder.helper.MinMaxInputFilter;
import at.truetigger.birthdayreminder.widget.BirthdayWidget;
import at.truetigger.birthdayreminder.widget.WidgetBackground;
import at.truetigger.birthdayreminder.widget.WidgetConfig;
import at.truetigger.birthdayreminder.widget.WidgetTheme;

public class ConfigureActivity extends AppCompatActivity {

    public static final String RECONFIGURE_WIDGET = "reconfigureWidget";

    public static final String SHARED_PREFS_BUNDLE = "at.truetigger.BirthdayReminder";
    public static final String KEY_DAYS_UNTIL = "dayUntil";
    public static final String KEY_THEME = "theme";
    public static final String KEY_BACKGROUND = "background";

    private static final int MIN_DAYS_UNTIL = 0;
    private static final int MAX_DAYS_UNTIL = 50;
    private static final int DEFAULT_DAYS_UNTIL = 7;

    private static final String TAG = "ConfigureActivity";

    private int widgetId;
    private boolean reconfigureActionFlag;

    private EditText editTextDaysForecast;
    private SeekBar selectForecastSeekBar;
    private RadioButton radioButtonThemeDark;
    private RadioButton radioButtonBackgroundTransparent;
    private RadioButton radioButtonBackgroundOpaque;
    private Button buttonConfigure;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        setResult(RESULT_CANCELED);
        extractWidgetId();
        detectReconfigureAction();
        WidgetConfig currentConfig = readWidgetConfig(widgetId);
        initComponents(currentConfig);
    }

    private void extractWidgetId() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        } else {
            widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
        }
        Log.d(TAG, "Extracted widgetId: " + widgetId);
    }

    private void detectReconfigureAction() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            reconfigureActionFlag = extras.getBoolean(RECONFIGURE_WIDGET, false);
        } else {
            reconfigureActionFlag = false;
        }
        Log.d(TAG, "ReconfigureAction: " + reconfigureActionFlag);
    }

    private WidgetConfig readWidgetConfig(int appWidgetId) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS_BUNDLE, Context.MODE_PRIVATE);
        final int daysUntil = sharedPreferences.getInt(ConfigureActivity.KEY_DAYS_UNTIL + appWidgetId, DEFAULT_DAYS_UNTIL);
        final WidgetTheme widgetTheme = WidgetTheme.valueOf(sharedPreferences.getString(ConfigureActivity.KEY_THEME + appWidgetId, WidgetTheme.LIGHT.name()));
        final WidgetBackground widgetBackground = WidgetBackground.valueOf(sharedPreferences.getString(ConfigureActivity.KEY_BACKGROUND + appWidgetId, WidgetBackground.TRANSPARENT.name()));
        WidgetConfig widgetConfig = new WidgetConfig(daysUntil, widgetTheme, widgetBackground);
        Log.d(TAG, "loaded widgetConfig for widgetId=" + appWidgetId + ": " + widgetConfig);
        return widgetConfig;
    }

    private void storeWidgetConfig() {
        if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            Log.d(TAG, "No valid widget ID, cannot store widget config");
            return;
        }

        storeSelectedConfig();

        Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE, null, this, BirthdayWidget.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        sendBroadcast(intent);
        Log.d(TAG, "Fire update via configuration for widget[" + widgetId + "]");

        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        setResult(RESULT_OK, resultValue);

        if (reconfigureActionFlag) {
            Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
            mainActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mainActivity.putExtra(MainActivity.CLOSE_IMMEDIATELY, true);
            startActivity(mainActivity);
        }
        finish();
    }

    private void storeSelectedConfig() {
        final int daysForecast = fetchSelectedForecast();
        final WidgetTheme theme = fetchSelectedTheme();
        final WidgetBackground background = fetchSelectedBackground();
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS_BUNDLE, MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putInt(KEY_DAYS_UNTIL + widgetId, daysForecast);
        sharedPreferencesEditor.putString(KEY_THEME + widgetId, theme.name());
        sharedPreferencesEditor.putString(KEY_BACKGROUND + widgetId, background.name());
        sharedPreferencesEditor.commit();
        Log.d(TAG, "Store config for widgetId=" + widgetId + ": days=" + daysForecast + ", theme=" + theme.name() + ", background=" + background.name());
    }

    private int fetchSelectedForecast() {
        return selectForecastSeekBar.getProgress();
    }

    private WidgetTheme fetchSelectedTheme() {
        WidgetTheme theme = radioButtonThemeDark.isChecked() ? WidgetTheme.DARK : WidgetTheme.LIGHT;
        Log.d(TAG, "Selected theme: " + theme);
        return theme;
    }

    private WidgetBackground fetchSelectedBackground() {
        WidgetBackground background = radioButtonBackgroundTransparent.isChecked()
                ? WidgetBackground.TRANSPARENT
                : radioButtonBackgroundOpaque.isChecked()
                    ? WidgetBackground.OPAQUE
                    : WidgetBackground.NONE;
        Log.d(TAG, "Selected background: " + background);
        return background;
    }

    private void initComponents(WidgetConfig widgetConfig) {

        editTextDaysForecast = findViewById(R.id.editDaysForecast);
        editTextDaysForecast.setFilters(new InputFilter[]{ new MinMaxInputFilter(MIN_DAYS_UNTIL, MAX_DAYS_UNTIL)} );
        editTextDaysForecast.setText(String.valueOf(widgetConfig.getDaysUntil()));

        selectForecastSeekBar = findViewById(R.id.forecastBar);
        selectForecastSeekBar.setMax(MAX_DAYS_UNTIL - MIN_DAYS_UNTIL);
        selectForecastSeekBar.setProgress(widgetConfig.getDaysUntil() - MIN_DAYS_UNTIL);

        radioButtonThemeDark = findViewById(R.id.themeDark);
        radioButtonThemeDark.setChecked(widgetConfig.getTheme() == WidgetTheme.DARK);
        radioButtonBackgroundTransparent = findViewById(R.id.backgroundTransparent);
        radioButtonBackgroundOpaque = findViewById(R.id.backgroundOpaque);
        switch (widgetConfig.getBackground()) {
            case TRANSPARENT:
                radioButtonBackgroundTransparent.setChecked(true);
                break;
            case OPAQUE:
                radioButtonBackgroundOpaque.setChecked(true);
                break;
        }

        buttonConfigure = findViewById(R.id.btnConfigure);
        buttonConfigure.setOnClickListener(view -> { storeWidgetConfig(); });

        editTextDaysForecast.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    int value = Integer.parseInt(editable.toString());
                    Log.d(TAG, "New entered text: " + value);
                    selectForecastSeekBar.setProgress(value - MIN_DAYS_UNTIL);
                } catch (NumberFormatException nfe) {
                    Log.w(TAG, String.format("Cannot convert %s into number: %s", editable.toString(), nfe));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        });

        selectForecastSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d(TAG, "New seekbar value: " + progress);
                String progressAsText = String.valueOf(progress + MIN_DAYS_UNTIL);
                editTextDaysForecast.setText(progressAsText);
                editTextDaysForecast.setSelection(progressAsText.length());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }
}
