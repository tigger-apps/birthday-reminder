package at.truetigger.birthdayreminder.activities;

import android.Manifest;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import at.truetigger.birthdayreminder.R;
import at.truetigger.birthdayreminder.helper.BirthdayArrayAdapter;
import at.truetigger.birthdayreminder.helper.BirthdayDataExtractor;
import at.truetigger.birthdayreminder.model.BirthdayRecord;
import at.truetigger.birthdayreminder.widget.BirthdayWidget;

public class MainActivity extends AppCompatActivity
        implements ActivityCompat.OnRequestPermissionsResultCallback {

    public static final String CLOSE_IMMEDIATELY = "CLOSE_APP";

    private static final int PERMISSION_REQUEST_READ_CONTACTS = 1;

    private static final String TAG = "MainActivity";

    private Button startButton;
    private Button clearButton;
    private ProgressBar progressBar;

    List<BirthdayRecord> birthdayRecords;
    ListView listView;
    private static BirthdayArrayAdapter adapter;

    private BirthdayDataExtractor birthdayDataExtractor = new BirthdayDataExtractor();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getBooleanExtra(CLOSE_IMMEDIATELY, false)) {
            updateWidget();
            finish();
        }
        setContentView(R.layout.activity_main);
        initComponents();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                readContacts();
            } else {
                Toast.makeText(this, getResources().getString(R.string.toast_permission_denied), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initComponents() {
        startButton = findViewById(R.id.btnStart);
        startButton.setOnClickListener(view -> start());
        clearButton = findViewById(R.id.btnClear);
        clearButton.setOnClickListener(view -> clear());
        progressBar = findViewById(R.id.progressBar);
        listView = findViewById(R.id.listRecords);
        birthdayRecords = new ArrayList<>();
        adapter = new BirthdayArrayAdapter(birthdayRecords, getApplicationContext());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {

            BirthdayRecord birthdayRecord = birthdayRecords.get(position);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, birthdayRecord.getContactId());
            intent.setData(uri);
            startActivity(intent);
        });
    }

    private void clear() {
        birthdayRecords.clear();
        notifyDataSetChanged();
    }

    private void notifyDataSetChanged() {
        runOnUiThread(() -> adapter.notifyDataSetChanged());
    }
    private void start() {
        clear();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {
            readContacts();
        } else {
            // Permission is missing and must be requested.
            requestReadContactPermissions();
            updateWidget();
        }
    }

    private void requestReadContactPermissions() {
        Toast.makeText(this, getResources().getString(R.string.toast_permission_requested), Toast.LENGTH_SHORT).show();
        ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    PERMISSION_REQUEST_READ_CONTACTS);
    }

    private void readContacts() {
        AsyncTaskRunner runner = new AsyncTaskRunner();
        runner.execute(new String());
    }

    private void updateWidget() {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        int[] widgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(this, BirthdayWidget.class));
        Intent updateIntent = new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIds);
        sendBroadcast(updateIntent);
    }

    private class AsyncTaskRunner extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            long tsStart = System.currentTimeMillis();
            ZonedDateTime now = ZonedDateTime.now();

            for (BirthdayRecord birthdayRecord : birthdayDataExtractor.extractBirthdayFromContacts(getContentResolver())) {
                birthdayRecords.add(birthdayRecord);
            };
            long tsEnd = System.currentTimeMillis();
            double seconds = (tsEnd - tsStart) / 1000d;
            String timeConsumed = String.format("%.2f", seconds);
            Log.d(TAG, "Read all contacts from " + tsStart + " to " + tsEnd + "(" + timeConsumed + "sec)");

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            notifyDataSetChanged();
            progressBar.setVisibility(View.INVISIBLE);
            updateWidget();
        }

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }
    }
}
