package at.truetigger.birthdayreminder.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.time.ZonedDateTime;
import java.util.List;

import at.truetigger.birthdayreminder.model.BirthdayRecord;
import at.truetigger.birthdayreminder.R;

public class BirthdayArrayAdapter extends ArrayAdapter<BirthdayRecord> implements View.OnClickListener {

    private final Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView nameTextView;
        TextView daysLeftTextView;
        TextView dateTextView;
    }

    public BirthdayArrayAdapter(List<BirthdayRecord> data, Context context) {
        super(context, R.layout.birthdaylist_record, data);
        this.mContext=context;
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ZonedDateTime now = ZonedDateTime.now();
        BirthdayRecord birthdayRecord = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.birthdaylist_record, parent, false);
            viewHolder.nameTextView = (TextView) convertView.findViewById(R.id.bdListName);
            viewHolder.daysLeftTextView = (TextView) convertView.findViewById(R.id.bdListDaysLeft);
            viewHolder.dateTextView = (TextView) convertView.findViewById(R.id.bdListDate);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.nameTextView.setText(birthdayRecord.getContactName());
        viewHolder.daysLeftTextView.setText(BirthdayFormatter.formatDaysLeft(birthdayRecord.getDaysUntil(now), mContext));
        viewHolder.dateTextView.setText(BirthdayFormatter.formatDateAndAge(birthdayRecord, now, mContext));
        viewHolder.nameTextView.setOnClickListener(this);
        viewHolder.nameTextView.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }
}
