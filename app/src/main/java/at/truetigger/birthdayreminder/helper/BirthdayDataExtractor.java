package at.truetigger.birthdayreminder.helper;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import at.truetigger.birthdayreminder.model.BirthdayRecord;

public class BirthdayDataExtractor {

    private static String[] BIRTHDAY_DETAILS_COLUMNS = {
            ContactsContract.CommonDataKinds.Event.START_DATE,
            ContactsContract.CommonDataKinds.Event.TYPE,
            ContactsContract.CommonDataKinds.Event.MIMETYPE,
    };
    private static String[] BIRTHDAY_SELECTION_ARGS = null;
    private static String BIRTHDAY_SORT_ORDER = ContactsContract.Contacts.DISPLAY_NAME;

    private static String TAG = "BirthdayDataExtractor";

    public List<BirthdayRecord> extractBirthdayFromContacts(@NonNull ContentResolver contentResolver) {
        ArrayList<BirthdayRecord> result = new ArrayList<>();

        Cursor contactsCursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (contactsCursor != null) {

            while (contactsCursor.moveToNext()) {
                String contactId = contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                String where = ContactsContract.CommonDataKinds.Event.TYPE + "=" + ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY
                        + " and " + ContactsContract.CommonDataKinds.Event.MIMETYPE + " = '" + ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE + "'"
                        + " and " + ContactsContract.Data.CONTACT_ID + " = " + contactId;

                Cursor birthdayCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, BIRTHDAY_DETAILS_COLUMNS, where, BIRTHDAY_SELECTION_ARGS, BIRTHDAY_SORT_ORDER);
                if (birthdayCursor != null) {
                    while (birthdayCursor.moveToNext()) {
                        String birthdayAsStr = birthdayCursor.getString(birthdayCursor.getColumnIndex(ContactsContract.CommonDataKinds.Event.START_DATE));
                        ContactDate birthday = new ContactDate(birthdayAsStr);
                        BirthdayRecord birthdayRecord = new BirthdayRecord(contactId, contactName, birthday.getMonth(), birthday.getDayOfMonth(), birthday.getYear());
                        result.add(birthdayRecord);
                        Log.d(TAG, "Added record: " + birthdayRecord);
                    }
                    birthdayCursor.close();
                }
            }
            contactsCursor.close();
        }

        Collections.sort(result);
        return result;
    }

    private class ContactDate {
        private static final String SPLIT_AT = "-";
        private final Integer month;
        private final Integer dayOfMonth;
        private final Integer year;

        public ContactDate(String dateAsRfc8601) {
            String[] parts = dateAsRfc8601.split(SPLIT_AT);
            if (parts.length == 4) {
                // no date is marked as --<month>-day
                year = null;
                month = Integer.valueOf(parts[2]);
                dayOfMonth = Integer.valueOf(parts[3]);
            } else if (parts.length != 3) {
                throw new IllegalArgumentException("Expecting format <year>-<month>-<day>, input: " + dateAsRfc8601);
            } else {
                year = Integer.valueOf(parts[0]);
                month = Integer.valueOf(parts[1]);
                dayOfMonth = Integer.valueOf(parts[2]);
            }
        }

        public Integer getMonth() {
            return month;
        }

        public Integer getDayOfMonth() {
            return dayOfMonth;
        }

        public Integer getYear() {
            return year;
        }
    }
}
