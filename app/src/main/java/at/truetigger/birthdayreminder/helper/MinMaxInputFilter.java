package at.truetigger.birthdayreminder.helper;

import android.text.InputFilter;
import android.text.Spanned;

public class MinMaxInputFilter implements InputFilter {

    private final int minAllowedValue;
    private final int maxMaxAllowedValue;

    public MinMaxInputFilter(int minAllowedValue, int maxAllowedValue) {
        this.minAllowedValue = minAllowedValue;
        this.maxMaxAllowedValue = maxAllowedValue;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            int input = Integer.parseInt(dest.toString() + source.toString());
            if (isInRange(input)) {
                return null;
            }
        } catch (NumberFormatException nfe) {
        }

        return ""; // not allowed
    }

    private boolean isInRange(int input) {
        return input >= minAllowedValue && input <= maxMaxAllowedValue;
    }
}
