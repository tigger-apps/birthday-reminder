package at.truetigger.birthdayreminder.helper;

import android.content.Context;
import android.text.format.DateUtils;

import androidx.annotation.NonNull;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Optional;

import at.truetigger.birthdayreminder.model.BirthdayRecord;
import at.truetigger.birthdayreminder.R;

public class BirthdayFormatter {

    public static @NonNull String formatDaysLeft(int daysLeft, Context context) {
        if (daysLeft == 0) {
            return context.getResources().getString(R.string.today);
        }
        if (daysLeft == 1) {
            return  context.getResources().getString(R.string.tomorrow);
        }
        return String.format(context.getResources().getString(R.string.days_left), daysLeft);
    }

    public static @NonNull String formatDate(@NonNull BirthdayRecord birthdayRecord,
                                      @NonNull ZonedDateTime now,
                                      @NonNull Context context) {
        return formatDateAsString(birthdayRecord, now, context);
    }

    public static @NonNull String formatDateAndAge(@NonNull BirthdayRecord birthdayRecord,
                                            @NonNull ZonedDateTime now,
                                            @NonNull Context context) {
        return formatDateAsString(birthdayRecord, now, context) + formatAgeAsString(birthdayRecord, now, context);
    }

    private static @NonNull String formatDateAsString(@NonNull BirthdayRecord birthdayRecord,
                                                      @NonNull ZonedDateTime now,
                                                      @NonNull Context context) {
        LocalDateTime birthdayDateLocal = LocalDateTime.of(now.getYear(), birthdayRecord.getMonth(), birthdayRecord.getDayOfMonth(), now.getHour(), now.getMinute());
        long millis = birthdayDateLocal.atZone(now.getZone()).toInstant().toEpochMilli();
        return DateUtils.formatDateTime(context, millis, DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_MONTH | DateUtils.FORMAT_NO_YEAR);
    }

    private static @NonNull String formatAgeAsString(@NonNull BirthdayRecord birthdayRecord,
                                                     @NonNull ZonedDateTime now, Context context) {
        Optional<Integer> age = birthdayRecord.getAge(now);
        if (age.isPresent()) {
            return " (" + context.getResources().getString(R.string.age) + ": " + age.get() + ")";
        }
        return "";
    }
}
