package at.truetigger.birthdayreminder.model;

import android.util.Log;

import androidx.annotation.NonNull;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class BirthdayRecord implements Comparable<BirthdayRecord> {

    private static final String TAG = "BirthdayRecord";
    private static final ZonedDateTime REFERENCE_TIME = ZonedDateTime.now();

    private final String contactId;
    private final String contactName;
    private final Integer month;
    private final Integer dayOfMonth;
    private final Integer year;

    public BirthdayRecord(@NonNull String contactId,
                          @NonNull String contactName,
                          @NonNull Integer month,
                          @NonNull Integer dayOfMonth,
                          Integer year) {
        this.contactId = contactId;
        this.contactName = contactName;
        this.month = month;
        this.dayOfMonth = dayOfMonth;
        this.year = year;
    }

    public String getContactId() {
        return contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public Integer getMonth() {
        return month;
    }

    public Integer getDayOfMonth() {
        return dayOfMonth;
    }

    public Optional<Integer> getYear() {
        return Optional.ofNullable(year);
    }

    public Integer getDaysUntil(@NonNull ZonedDateTime currentTimestamp) {
        final int currentDayOfYear = currentTimestamp.getDayOfYear();

        ZonedDateTime nextBirthdayZoned = buildCurrentYearsBirthdate(currentTimestamp);
        int daysTillNextBirthday = daysBetweenTimestamps(currentTimestamp, nextBirthdayZoned);
        if (daysTillNextBirthday < 0) {
            daysTillNextBirthday = daysBetweenTimestamps(currentTimestamp, nextBirthdayZoned.plusYears(1));
        }

        return Math.toIntExact(daysTillNextBirthday);
    }

    public Optional<Integer> getAge(ZonedDateTime currentTimestamp) {
        if (!getYear().isPresent()) {
            return Optional.empty();
        }

        ZonedDateTime thisYearsBirthdayZoned = buildCurrentYearsBirthdate(currentTimestamp);
        int age = thisYearsBirthdayZoned.getYear() - getYear().get();
        if (thisYearsBirthdayZoned.getDayOfYear() < currentTimestamp.getDayOfYear()) {
            ++age; // this years birthday is over, use next years birthday
        }
        return Optional.of(age);
    }

    @NonNull
    @Override
    public String toString() {
        return "BirthdayRecord[id=" + contactId + ", name=" + contactName + ", month/day=" + month + "/" + dayOfMonth + ", year=" + year + "]";
    }

    @Override
    public int compareTo(@NonNull BirthdayRecord otherBirthdayRecord) {
        return Integer.compare(getDaysUntil(REFERENCE_TIME), otherBirthdayRecord.getDaysUntil(REFERENCE_TIME));
    }

    private ZonedDateTime buildCurrentYearsBirthdate(@NonNull ZonedDateTime currentTimestamp) {
        LocalDateTime nextBirthdayLocal = LocalDateTime.of(currentTimestamp.getYear(), getMonth(), getDayOfMonth(),
                currentTimestamp.getHour(), currentTimestamp.getMinute(), currentTimestamp.getSecond(), currentTimestamp.getNano());
        return ZonedDateTime.of(nextBirthdayLocal, currentTimestamp.getZone());
    }

    private int daysBetweenTimestamps(ZonedDateTime firstTimestamp, ZonedDateTime secondTimestamp) {
        Log.d(TAG, "firstTimestamp: " + firstTimestamp + ", secondTimestamp: " + secondTimestamp);
        long differenceInDays = ChronoUnit.DAYS.between(firstTimestamp, secondTimestamp);
        Log.d(TAG, "Difference in days: " + differenceInDays);
        return Math.toIntExact(differenceInDays);
    }
}
