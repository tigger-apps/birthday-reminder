package at.truetigger.birthdayreminder;

import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.Optional;

import at.truetigger.birthdayreminder.model.BirthdayRecord;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BirthdayRecordTest {

    private static final String CONTACT_ID = "12345";
    private static final String CONTACT_NAME = "Amy Winehouse";
    private static final int BIRTH_DAY = 14;
    private static final int BIRTH_MONTH = 9;
    private static final int BIRTH_YEAR = 1984;

    private static final ZonedDateTime CURRENT_TIMESTAMP = ZonedDateTime.parse("2020-10-18T17:08:11.251+02:00[Europe/Vienna]");

    @Test
    public void returnDaysTillThisYearsBirthdayBeforeBirthdayIsOver() {
        ZonedDateTime beforeBirthday = ZonedDateTime.parse("2001-09-02T17:08:11.251+02:00[Europe/Vienna]");
        BirthdayRecord birthdayRecord = new BirthdayRecord(CONTACT_ID, CONTACT_NAME, BIRTH_MONTH, BIRTH_DAY, null);

        Integer result = birthdayRecord.getDaysUntil(beforeBirthday);

        assertThat("Expect 11 days between Sept 2nd and Sep 14th", result, is(12));
    }

    @Test
    public void returnZeroDaysTillOnBirthdayItself() {
        ZonedDateTime onBirthday = ZonedDateTime.parse("1999-09-14T01:00:00.000+00:00[GMT]");
        BirthdayRecord birthdayRecord = new BirthdayRecord(CONTACT_ID, CONTACT_NAME, BIRTH_MONTH, BIRTH_DAY, null);

        Integer result = birthdayRecord.getDaysUntil(onBirthday);

        assertThat("Expect zero days on birthday itself", result, is(0));
    }

    @Test
    public void returnMaxDaysTillAfterBirthdayOnNormalYear() {
        ZonedDateTime afterBirthday = ZonedDateTime.parse("2004-09-15T01:00:00.000+00:00[GMT]"); // 2005 was a normal year
        BirthdayRecord birthdayRecord = new BirthdayRecord(CONTACT_ID, CONTACT_NAME, BIRTH_MONTH, BIRTH_DAY, null);

        Integer result = birthdayRecord.getDaysUntil(afterBirthday);

        assertThat("Expect 364 days till next birthday on normal year", result, is(364));
    }

    @Test
    public void returnMaxDaysTillAfterBirthdayOnLeapYear() {
        ZonedDateTime afterBirthdayLeapYear = ZonedDateTime.parse("2003-09-15T01:00:00.000+00:00[GMT]"); // 2004 was a leap year
        BirthdayRecord birthdayRecord = new BirthdayRecord(CONTACT_ID, CONTACT_NAME, BIRTH_MONTH, BIRTH_DAY, null);

        Integer result = birthdayRecord.getDaysUntil(afterBirthdayLeapYear);

        assertThat("Expect 365 days till next birthday on leap year", result, is(365));
    }

    @Test
    public void returnNoAgeWithoutYear() {
        BirthdayRecord birthdayRecord = new BirthdayRecord(CONTACT_ID, CONTACT_NAME, BIRTH_MONTH, BIRTH_DAY, null);

        Optional<Integer> result = birthdayRecord.getAge(ZonedDateTime.now());

        assertThat("Expect empty age without set year", result.isPresent(), is(false));
    }

    @Test
    public void returnAgeBeforeBirthday() {
        ZonedDateTime beforeBirthday = ZonedDateTime.parse("2001-08-17T17:11:14.452-04:00[America/New_York]");
        BirthdayRecord birthdayRecord = new BirthdayRecord(CONTACT_ID, CONTACT_NAME, BIRTH_MONTH, BIRTH_DAY, BIRTH_YEAR);

        Optional<Integer> result = birthdayRecord.getAge(beforeBirthday);

        assertThat("Amy was 17 on Aug 17th 2001", result.get(), is(17));
    }

    @Test
    public void returnAgeOnBirthday() {
        ZonedDateTime beforeBirthday = ZonedDateTime.parse("2002-09-14T23:58:14.452-04:00[America/New_York]");
        BirthdayRecord birthdayRecord = new BirthdayRecord(CONTACT_ID, CONTACT_NAME, BIRTH_MONTH, BIRTH_DAY, BIRTH_YEAR);

        Optional<Integer> result = birthdayRecord.getAge(beforeBirthday);

        assertThat("Amy was 18 on her birthday 2001", result.get(), is(18));
    }

    @Test
    public void returnAgeAfterBirthday() {
        ZonedDateTime beforeBirthday = ZonedDateTime.parse("2003-09-15T00:00:01.452-04:00[America/New_York]");
        BirthdayRecord birthdayRecord = new BirthdayRecord(CONTACT_ID, CONTACT_NAME, BIRTH_MONTH, BIRTH_DAY, BIRTH_YEAR);

        Optional<Integer> result = birthdayRecord.getAge(beforeBirthday);

        assertThat("Amy was awaiting her 20th birthday 2003-09-15", result.get(), is(20));
    }
}
